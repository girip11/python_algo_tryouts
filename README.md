# Algorithms tryout in python

Code for various algorithms implemented in python. Mainly aimed at getting very good in algorithms and data structures.

## Data structures and algorithms books

* [Problem Solving with Algorithms and Data Structures using Python](https://runestone.academy/runestone/books/published/pythonds/index.html)
* [Data Structure and Algorithmic Thinking with Python](https://www.amazon.in/Data-Structure-Algorithmic-Thinking-Python/dp/8192107590)
