# Binary string of length n using recursion and backtracking

from typing import List

# 1. Using multiple recursion


def binary_strings(n: int) -> List[str]:
    if n == 0:
        return []
    if n == 1:
        return ["0", "1"]
    return list("0" + b for b in binary_strings(n - 1)) + list(
        "1" + b for b in binary_strings(n - 1)
    )


print(binary_strings(3))


def append(bit: str, acc: List[str]) -> List[str]:
    if acc == []:
        return [bit]
    return [bit + bitstring for bitstring in acc]


def binary_strings_tail_recursive(n: int, acc: List[str] = []) -> List[str]:
    if n == 0:
        return acc
    return binary_strings_tail_recursive(n - 1, append("0", acc) + append("1", acc))


print(binary_strings_tail_recursive(3))
