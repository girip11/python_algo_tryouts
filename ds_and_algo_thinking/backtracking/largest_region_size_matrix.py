# Find the largest connected region in a matrix
# Matrix filled with 0 and 1s
# If a cell contains 1, then it is a valid cell
# If a cell contains another adjacent cell with value 1, then
# these two cells are said to be connected.
# Find the size of the largest connected region
# Example
# 1*  1*  0   0   0
# 0   1*  1*  0   0
# 0   0   1*  0   1
# 1   0   0   0   1
# 0   1   0   1   1
# Region with max size is 5

from typing import Iterator, List, Tuple

# Solving this problem involves finding largest region of its neighbours
# Given matrix(i,j) = 1
# LargestRegion(i,j) = 1 + max(LargestRegion of all the neighbours of (i,j))
# Base case is when matrix(i,j) = 0 as well as the recursion terminating case
# is when all the neighbours of a cell are already visited and no more new
# cells to explore.


def find_largest_region(matrix: List[List[int]]) -> int:
    if is_empty(matrix):
        return 0

    max_size: int = 0
    rows = len(matrix)

    for row in range(0, rows):
        for col in range(0, len(matrix[row])):
            if matrix[row][col] != 0:
                max_size = max(max_size, get_largest_region_size(matrix, row, col, []))
                # c = input("wait here")

    return max_size


def is_empty(matrix: List[List[int]]) -> bool:
    return not any(matrix)


def get_largest_region_size(
    matrix: List[List[int]], i: int, j: int, visited_cells: List[Tuple[int, int]]
) -> int:
    if matrix[i][j] == 0:
        return 0

    max_size: int = 0
    directions = list(
        filter(
            lambda e: e not in visited_cells,
            get_directions(i, j, len(matrix), len(matrix[i])),
        )
    )
    # print(f"r: {i}, c:{j}, cells: {visited_cells}, dir: {directions}")
    # c = input("wait here")

    visited_cells.append((i, j))
    for rd, cd in directions:  # get_directions(i, j, len(matrix), len(matrix[i])):
        if (rd, cd) not in visited_cells:
            max_size = max(
                max_size, get_largest_region_size(matrix, rd, cd, visited_cells)
            )

    return max_size + 1


def get_directions(i, j, n, m) -> Iterator[Tuple[int, int]]:
    directions: List[Tuple[int, int]] = [
        (0, -1),  # left
        (-1, -1),  # left up diagonal
        (-1, 0),  # up
        (-1, 1),  # right up diagonal
        (0, 1),  # right
        (1, 1),  # right down diagonal
        (1, 0),  # down
        (1, -1),  # left down diagonal
    ]

    return filter(
        lambda e: (0 <= e[0] < n) and (0 <= e[1] < m),
        map(lambda e: (e[0] + i, e[1] + j), directions),
    )


input_matrix = [
    [1, 1, 0, 0, 0],
    [0, 1, 1, 0, 0],
    [0, 0, 1, 0, 1],
    [1, 0, 0, 0, 1],
    [0, 1, 0, 1, 1],
]

print(find_largest_region(matrix=input_matrix) == 5)


input_matrix = [
    [1, 1, 1, 1, 1],
    [1, 0, 0, 0, 1],
    [1, 0, 0, 0, 1],
    [1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1],
]
print(find_largest_region(matrix=input_matrix) == 16)

input_matrix = [
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
]
print(find_largest_region(matrix=input_matrix) == 25)
