# This is a generalization of the binary strings problem
# given n, generate all strings of length n, composed of digits from the base K
# if k = 3 and n = 1, we get [0,1,2]
# if k = 3 and n = 2, we get [00, 01, 02, 10, 11, 12, 20, 21, 22]
# We should finally get k^n values

from typing import List
from itertools import product


# Solution explanation
# ======================
# consider k(base) = 3 n = 2
# 0 + f(n-1, k) => 0 + [one of 0,1,2]
# similarly we have to do the above step with 1 as the prefix. 1 + [one of 0,1,2]
# similarly for 2 => 2 + [one of 0,1,2]

# Suppore base = 3 and n = 3
# 1. {0 + (select two values from 0, 1, 2)} +
#    {1 + (select two values from 0, 1, 2)} +
#    {2 + (select two values from 0, 1, 2)}
#
# First part of the above step can be expanded as
#
# 2. => 0 + {0 + (select one value from 0, 1, 2)} +
#           {1 + (select one value from 0, 1, 2)} +
#           {2 + (select one value from 0, 1, 2)}
#
# 3. 0 + {{[00, 01, 02] } + {[10, 11, 12]} +  {[20, 21, 22]}}
#
# Thus we get ['000', '001', '002', '010', '011', '012', '020', '021', '022']
# Similarly for the other parts also we get
# ['100', '101', '102', '110', '111', '112', '120', '121', '122']
# ['200', '201', '202', '210', '211', '212', '220', '221', '222']

# This involves two loops
# 1. Iterate through all the values of K as prefixes and
# 2. Find the rest of the numbers for each fixed prefix (this is the subproblem)


def nary_strings(n: int, k: int) -> List[str]:
    if n == 0:
        return []

    if n == 1:
        return list(map(str, range(0, k)))

    return [
        digit + entry
        for digit in nary_strings(1, k)
        for entry in nary_strings(n - 1, k)
    ]


product_result = ["".join(map(str, i)) for i in product(range(0, 3), repeat=3)]
print(nary_strings(3, 3) == product_result)


# Lets write a tail recursive definition to compute
# the combinations of all digits of a fixed length n


def nary_strings_tail_recursive(
    n: int, base: int, k: int = 0, acc: List[str] = []
) -> List[str]:
    # This will be my terminating condition for the tail recursive call
    if k >= base:
        return acc

    # this is to handle this method call with n = 0
    # recursion will not hit this condition
    if n == 0:
        return []

    # This is to stop the recursion call inside the list
    # comprehension below
    if n == 1:
        return [str(i) for i in range(0, base)]

    current_combinations = [
        str(k) + entry for entry in nary_strings_tail_recursive(n - 1, base, 0, [])
    ]

    # print(current_combinations)

    return nary_strings_tail_recursive(n, base, k + 1, acc + current_combinations)


print(nary_strings_tail_recursive(3, 3))
