# Find and return a path from source to destination
# Source is the first cell in the path matrix
# destination in the last cell in the path matrix
# From a cell, we can either move right or down.
# A cell with value 0 is a wall and a cell with value 1 represents a path
# Example
# 1*  1*  0   0
# 0   1*  1*  0
# 0   0   1*  0
# 1   0   1*  1*
# Path - [(0,0), (0,1), (1,1), (1,2), (2,2), (3,2), (3,3)]

from typing import List, Tuple


def find_path(path_matrix: List[List[int]], i: int, j: int) -> List[Tuple[int, int]]:
    # if its a wall return no path to the destination
    if path_matrix[i][j] == 0:
        return []

    # If the current cell is the destination then return itself
    # since the destination is reachable from itself
    if i == len(path_matrix) - 1 and j == len(path_matrix[i]) - 1:
        return [(i, j)]

    # outside the path matrix
    if i >= len(path_matrix) or j >= len(path_matrix[i]):
        return []

    path_to_destination = find_path(path_matrix, i, j + 1) or find_path(
        path_matrix, i + 1, j
    )

    if path_to_destination != []:
        path_to_destination.append((i, j))

    return path_to_destination


input_matrix = [[1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 1, 0], [0, 0, 1, 1]]
path = [(0, 0), (0, 1), (1, 1), (1, 2), (2, 2), (3, 2), (3, 3)]

print(list(reversed(find_path(input_matrix, 0, 0))) == path)
