# Get all the strings of length n from an input string s of length k
# such that n <= k

# For instance, if s is "abcde" and n = 3 then the result should return
# [abc, abd, abe, acd, ace, ade, bcd, bce, bde, cde] 5C3

from typing import List

import itertools

# Thought process
# s = "abcde" n=3
# a + sub(n=2, s=bcde)
# 1. a + b + sub(n=1, s=cde)
# a + b + [c,d,e](base case result)
# a + [bc, bd, be]
# [abc, abd, abe]
# Now the step 1 will continue as
# a + c + sub(n=1,s=de)

def combinations(s: str, n: int) -> List[str]:
    if n == 0:
        return [""]

    return [
        s[i] + entry
        for i in range(0, len(s) - n + 1)
        for entry in combinations(s[(i + 1) :], n - 1)
    ]


itertools_result = ["".join(e) for e in itertools.combinations("abcde", 2)]
print(combinations("abcde", 2) == itertools_result)


# This is an example of nested recursion as well as tail recursion
# Observe how the acc parameter in the tail recursive all involves the
# result from another tail recursive call which is stored in
# the variable current_combinations
def combinations_tail_recursive(s: str, n: int, acc: List[str]) -> List[str]:
    # This check is for terminating the tail recursive call
    if len(s) < n:
        return acc

    if n == 0:
        return []

    # This call is to terminate the combination computation part
    # which is invoked inside the below list comprehension
    if n == 1:
        return list(s)

    # For every first character, find the next n-1 characters from s
    current_combinations = [
        s[0] + entry for entry in combinations_tail_recursive(s[1:], n - 1, [])
    ]

    # This call iterates through every character in the string s
    return combinations_tail_recursive(s[1:], n, acc + current_combinations)


print(combinations("abcde", 2))
print(combinations_tail_recursive("abcde", 2, []))
