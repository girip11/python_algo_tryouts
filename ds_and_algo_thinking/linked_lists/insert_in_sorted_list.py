# Insert an item in to a sorted singly linked list

from typing import Optional

from ds_and_algo_thinking.linked_lists.singly_linked_list import Node


def insert(head: Optional[Node], item: int) -> Node:
    if head is None:
        return Node(item, head)

    prev: Optional[Node] = None
    current: Optional[Node] = head

    while current is not None and current.data <= item:
        prev = current
        current = current.next_node

    if prev is None:
        return Node(item, head)

    node_to_insert = Node(item, current)
    prev.next_node = node_to_insert

    return head


input = Node(1, Node(2, Node(3, Node(5, None))))
print(insert(input, 4))

print(insert(None, 4))

input2 = Node(5, None)
print(insert(input2, 4))
print(insert(input2, 6))
