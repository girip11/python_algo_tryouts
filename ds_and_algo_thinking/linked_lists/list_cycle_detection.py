# Detect if a singly linked list contains a loop


class CycleDetector:
    def __init__(self, linked_list):
        self.head = linked_list
        self.loop_exists = None
        self.loop_node = None
        self.loop_length = -1

    def is_loop_present(self) -> bool:
        # result caching
        if self.loop_exists is None:
            self.loop_exists = self._is_loop_present()

        return self.loop_exists

    def _is_loop_present(self) -> bool:
        if self.head is None:
            return False

        slow = fast = self.head

        while fast is not None and fast.next_node is not None and fast != slow:
            fast = fast.next_node.next_node
            slow = slow.next_node

        if fast == slow:
            self.loop_node = slow

        return fast is slow

    def find_loop_length(self) -> int:  # sourcery off
        if self.is_loop_present() and self.loop_length < 0:
            itr = self.loop_node
            self.loop_length = 1
            while itr != self.loop_node:
                itr = itr.next_node
                self.loop_length += 1

        return self.loop_length

    def find_loop_origin_node(self):
        if not self.is_loop_present():
            return None

        itr1 = self.head
        itr2 = self.loop_node

        while itr1 is not itr2:
            itr1 = itr1.next_node
            itr2 = itr2.next_node

        return itr1
