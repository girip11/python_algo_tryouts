# Find the nth element from the end of the singly linked list

# approaches
# 1. Reverse the linked list. Iterate through the reversed list and return the nth element
# This takes O(n) space and required 2 traversals

# 2. Iterate once and find the length as well as create a hash table of position to element.
# Using the length and n, compute the position of the element from the
# starting which is (length -n + 1). This approach takes O(n) space and one traversal

# 3. Iterate once and find the length. Using the formula (m = length - n + 1),
# iterate again and return the mth element from the
# beginning of the list. This requires us to scan the list twice, but constant space

# 4. Optimal solution -> Have two references pointing to head.
# The second one starts moving only after the
# first completes travering n elements in the list.
# Return the element pointed by the second reference
# when the first has reached the end of the list. One traversal and constant space.
# This is based on the idea by delaying second by n iterations,
# we have subtracted n from the length, so that the second can iterate
# the remaining distance from the beginning of the list

from typing import Optional

from ds_and_algo_thinking.linked_lists.singly_linked_list import (
    LinkedList, Node, get_linked_list)


def nth_from_last(linked_list: LinkedList, n: int) -> Optional[int]:
    if linked_list.head is None:
        return None

    itr: Node = linked_list.head

    for _ in range(n):
        if itr is None:
            break
        itr = itr.next_node

    # n is greater than the length of the list
    if itr is None:
        return None

    slow = linked_list.head
    while itr is not None:
        itr = itr.next_node
        slow = slow.next_node

    return slow.data


input_linked_list = get_linked_list(range(1, 8))
print(nth_from_last(input_linked_list, 3))
