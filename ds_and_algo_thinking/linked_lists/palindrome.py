# Check if the linked list is a palindrome or not

from typing import Optional, Tuple
from ds_and_algo_thinking.linked_lists.singly_linked_list import Node


def is_palindrome(head: Optional[Node]) -> bool:
    if head is None or head.next_node is None:
        return False

    middle, length = find_middle_node(head)

    reversed_list_head: Optional[Node] = reverse_list(
        middle if length % 2 == 0 else middle.next_node
    )
    palindrome: bool = False

    while head != middle and reversed_list_head is not None:
        if head.data != reversed_list_head.data:
            break
        head = head.next_node
        reversed_list_head = reversed_list_head.next_node

    return palindrome or (head == middle and reversed_list_head is None)


def find_middle_node(head: Optional[Node]) -> Tuple[Optional[Node], int]:
    if head is None:
        return (None, 0)

    slow_ptr: Optional[Node] = head
    fast_ptr: Optional[Node] = head
    count = 1

    while fast_ptr is not None and fast_ptr.next_node is not None:
        fast_ptr = fast_ptr.next_node.next_node
        slow_ptr = slow_ptr.next_node
        count += 2

    count -= 1 if fast_ptr is None else 0
    return (slow_ptr, count)


def reverse_list(head: Optional[Node]) -> Optional[Node]:
    def reverse_helper(head, acc=None):
        if head is None:
            return acc

        next_node = head.next_node
        head.next_node = acc

        return reverse_helper(next_node, head)

    return reverse_helper(head, None)


input = Node(1, Node(2, Node(3, Node(4, Node(5, None)))))
print(reverse_list(input))


input = Node(1, Node(2, Node(3, Node(2, Node(1, None)))))
print(is_palindrome(input))

input = Node(1, Node(2, Node(3, Node(2, Node(5, None)))))
print(is_palindrome(input))

input = Node(1, Node(2, Node(2, Node(1, None))))
print(is_palindrome(input))

input = Node(2, Node(2, None))
print(is_palindrome(input))
