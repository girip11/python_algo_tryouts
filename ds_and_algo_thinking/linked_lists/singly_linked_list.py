from dataclasses import dataclass
from typing import Iterable, List, Optional


@dataclass
class Node:
    data: int
    next_node: Optional["Node"]


class LinkedList:
    def __init__(self):
        self._head: Optional[Node] = None
        self._tail: Optional[Node] = None

    @property
    def head(self):
        return self._head

    @property
    def tail(self):
        return self._tail

    @staticmethod
    def get_new_node(data: int) -> Node:
        return Node(data, None)

    def insert_at_end(self, data):
        new_node = LinkedList.get_new_node(data)
        if self._head is None:
            self._head = new_node

        if self._tail is not None:
            self._tail.next_node = new_node

        self._tail = new_node

    def tolist(self) -> List[int]:
        result_list = []

        if self.head is not None:
            itr = self.head
            while itr is not None:
                result_list.append(itr.data)
                itr = itr.next_node

        return result_list


def get_linked_list(iterable: Iterable[int]) -> LinkedList:
    linked_list = LinkedList()
    for i in iterable:
        linked_list.insert_at_end(i)
    return linked_list


if __name__ == "__main__":
    linked_list = LinkedList()

    for i in range(5):
        linked_list.insert_at_end(i + 1)

    print(linked_list.tolist())
