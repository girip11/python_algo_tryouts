# Implement stack using singly linked list

# operations are push pop and peek

from typing import Optional

from .singly_linked_list import Node


class Stack:
    def __init__(self):
        self._head: Optional[Node] = None
        self._tail: Optional[Node] = None

    @staticmethod
    def get_new_node(data: int) -> Node:
        return Node(data, None)

    def push(self, data):
        new_node = Stack.get_new_node(data)
        if self._head is None:
            self._head = new_node

        if self._tail is not None:
            self._tail.next_node = new_node

        self._tail = new_node

    def peek(self) -> Optional[int]:
        return self._tail.data if self._tail else None

    # Complexity of this operation will be O(1) if implemented
    # using doubly linked list since tail would store the reference
    # to its previous node
    def pop(self) -> Optional[int]:
        if self._head is None:
            return None

        if self._head == self._tail:
            result = self._tail.data
            self._head = None
            self._tail = None
            return result

        itr: Node = self._head
        while itr.next_node != self._tail:
            itr = itr.next_node

        result = self._tail.data
        itr.next_node = None
        self._tail = itr
        return result


if __name__ == "__main__":
    stack = Stack()

    for i in range(5):
        stack.push(i + 1)

    print(stack.peek())

    for _ in range(5):
        print(stack.pop())
