from random import randint, seed
from typing import Iterable, List


def rand_index(max: int) -> int:
    return randint(0, max - 1)


# hill climbing approach
# start from the previous best
def generate_text_v2(allowed_characters: List[str], prev_gen_text: str, target_text: str) -> str:
    generated_characters = [
        allowed_characters[rand_index(len(allowed_characters))] if g != t else g
        for g, t in zip(prev_gen_text, target_text)
    ]

    return "".join(generated_characters)


def generate_text(allowed_characters: List[str], target_text: str) -> str:
    generated_characters = [
        allowed_characters[rand_index(len(allowed_characters))] for _ in range(0, len(target_text))
    ]

    return "".join(generated_characters)


def score(generated_text: Iterable[str], target_text: Iterable[str]) -> int:
    score = 0

    for g, t in zip(generated_text, target_text):
        if g == t:
            score += 1

    return score


def main() -> None:
    target_text = "methinks it is like a weasel"
    allowed_characters = [chr(c) for c in range(ord("a"), ord("z") + 1)] + [" "]
    best_score_so_far = 0
    max_iterations = 1000
    generated_text = "!" * 28
    seed(42)

    for i in range(0, max_iterations):
        # generated_text = generate_text(allowed_characters, target_text)
        generated_text = generate_text_v2(allowed_characters, generated_text, target_text)
        current_score = score(generated_text, target_text)

        if current_score > best_score_so_far:
            best_score_so_far = current_score
            print(f"New best score: {best_score_so_far}, iteration: {i}, text: {generated_text}")

        if best_score_so_far == len(target_text):
            break


if __name__ == "__main__":
    main()
