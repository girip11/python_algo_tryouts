def gcd(m, n):
    while m % n != 0:
        oldm = m
        oldn = n

        m = oldn
        n = oldm % oldn
        print(m, n)
    return n


class Fraction:
    def __init__(self, top, bottom):
        self.num = top
        self.den = bottom

    def __str__(self):
        return str(self.num) + "/" + str(self.den)

    def show(self):
        print(self.num, "/", self.den)

    def __add__(self, other_fraction: "Fraction") -> "Fraction":
        newnum = self.num * other_fraction.den + self.den * other_fraction.num
        newden = self.den * other_fraction.den
        common = gcd(newnum, newden)
        return Fraction(newnum // common, newden // common)

    def __sub__(self, other_fraction: "Fraction") -> "Fraction":
        newnum = self.num * other_fraction.den - self.den * other_fraction.num
        newden = self.den * other_fraction.den
        common = gcd(newnum, newden)
        return Fraction(newnum // common, newden // common)

    def __mul__(self, other_fraction: "Fraction") -> "Fraction":
        new_num = self.num * other_fraction.num
        new_den = self.den * other_fraction.den
        common_factor = gcd(new_num, new_den)
        return Fraction(new_num // common_factor, new_den // common_factor)

    def __truediv__(self, other_fraction: "Fraction") -> "Fraction":
        return self.__mul__(Fraction(other_fraction.den, other_fraction.num))

    def __eq__(self, other):
        firstnum = self.num * other.den
        secondnum = other.num * self.den

        return firstnum == secondnum


x = Fraction(1, 5)
y = Fraction(5, 8)
print(x + y)
print(x - y)
print(x * y)
print(x / y)
print(x == y)
