from random import randrange


class MultiSidedDie:
    def __init__(self, num_sides: int) -> None:
        self.num_sides = num_sides

    def roll(self) -> int:
        return randrange(1, self.num_sides + 1)

    def __str__(self) -> str:
        return f"Die with {self.num_sides} sides"

    def __repr__(self) -> str:
        return f"MultiSidedDie({self.num_sides})"


if __name__ == "__main__":
    die = MultiSidedDie(6)
    for _ in range(5):
        print(die.roll())
    print(str(die))
    print(repr(die))
