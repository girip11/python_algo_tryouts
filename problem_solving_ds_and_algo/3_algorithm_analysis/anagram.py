from collections import defaultdict
from typing import DefaultDict


def check_anagram_v1(text1: str, text2: str) -> bool:
    """Returns True if given input strings are anagrams.

    This function strips of any leading or trailing spaces before checking for anagram.
    This implementation is case sensitive.

    Args:
        text1 (str): [description]
        text2 (str): [description]

    Returns:
        bool: [description]
    """
    if (text1 is None or len(text1.strip()) == 0) or (text2 is None or len(text2.strip()) == 0):
        raise ValueError("Input strings cannot be None or Empty")

    text1 = text1.strip()
    text2 = text2.strip()
    if len(text1) != len(text2):
        return False

    # This implementation uses additional memory
    chars: DefaultDict[str, int] = defaultdict(int)
    for c in text1:
        chars[c] += 1

    for c in text2:
        if c not in chars:
            return False
        chars[c] += -1

    return all(map(lambda c: c == 0, chars.values()))
