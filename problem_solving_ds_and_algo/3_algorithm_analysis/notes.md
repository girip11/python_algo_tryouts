# Algorithm Analysis

* Algorithm analysis is concerned with comparing algorithms based upon the amount of computing resources that each algorithm uses.

* It turns out that the exact number of operations is not as important as determining the most dominant part of the `T(n)` function. In other words, as the problem gets larger, some portion of the `T(n)` function tends to overpower the rest. This dominant term is what, in the end, is used for comparison. The order of magnitude function describes the part of `T(n)` that increases the fastest as the value of `n` increases. Order of magnitude is often called Big-O notation (for “order”) and written as `O(f(n))`. It provides a useful approximation to the actual number of steps in the computation. The function `f(n)` provides a simple representation of the dominant part of the original `T(n)`.

* As an example, suppose that for some algorithm, the exact number of steps is `T(n)=5n2+27n+1005`. When n is small, say 1 or 2, the constant 1005 seems to be the dominant part of the function. However, as n gets larger, the n2 term becomes the most important. In fact, when n is really large, the other two terms become insignificant in the role that they play in determining the final result. Again, to approximate T(n) as n gets large, we can ignore the other terms and focus on 5n2. In addition, the coefficient 5 becomes insignificant as n gets large. We would say then that the function T(n) has an order of magnitude f(n)=n2, or simply that it is O(n2).

* Although we do not see this in the summation example, sometimes the performance of an algorithm depends on the exact values of the data rather than simply the size of the problem. For these kinds of algorithms we need to characterize their performance in terms of best case, worst case, or average case performance.

* On many occasions you will need to make decisions between time and space trade-offs. In this case, the amount of extra space is not significant. However, if the underlying alphabet had millions of characters, there would be more concern. As a computer scientist, when given a choice of algorithms, it will be up to you to determine the best use of computing resources given a particular problem.

---

## References

* [Big O Notation](https://runestone.academy/runestone/books/published/pythonds/AlgorithmAnalysis/BigONotation.html)
