from simple_deque import SimpleDeque


# This implementation uses additional memory in the form of deque
def is_palindrome(text: str, *, compare_case: bool = True) -> bool:
    if not text:
        raise ValueError("text is None or empty")

    if len(text) == 1:
        return True

    deque = SimpleDeque()
    for c in text.strip():
        deque.addRear(c if compare_case else c.lower())

    while deque.size() > 1:
        if deque.removeFront() != deque.removeRear():
            return False

    return True


if __name__ == "__main__":
    print(is_palindrome("Hello"))
    print(is_palindrome("malayalam"))
    print(is_palindrome("MALAyalam", compare_case=False))
