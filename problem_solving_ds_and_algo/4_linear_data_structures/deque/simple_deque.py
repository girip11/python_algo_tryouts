from abc import ABC, abstractmethod


class Deque(ABC):
    @abstractmethod
    def addFront(self, item):
        pass

    @abstractmethod
    def addRear(self, item):
        pass

    @abstractmethod
    def removeFront(self):
        pass

    @abstractmethod
    def removeRear(self):
        pass

    @abstractmethod
    def size(self) -> int:
        pass

    def isEmpty(self):
        return self.size() == 0


class SimpleDeque(Deque):
    def __init__(self):
        self._items = []

    def addFront(self, item):
        self._items.insert(0, item)

    def addRear(self, item):
        self._items.append(item)

    def removeFront(self):
        return self._items.pop(0)

    def removeRear(self):
        return self._items.pop()

    def size(self):
        return len(self._items)


if __name__ == "__main__":
    deque = SimpleDeque()
    deque.addFront(4)
    deque.addFront(11)
    deque.addRear(10)
    print(deque.removeFront())
    print(deque.removeRear())
