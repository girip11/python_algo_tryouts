# List datatype

* Linked lists are used for implementing ordered and unordered lists.

## Unordered list

### Abstract data type

* `add(item)`
* `remove(item)`
* `search(item)`
* `isEmpty()`
* `size()`
* `append(item)`
* `insert(pos,item)`
* `pop()`
* `pop(pos)`
* `index(item)`

## Ordered list

> The structure of an ordered list is a collection of items where each item holds a relative position that is based upon some underlying characteristic of the item. The ordering is typically either ascending or descending and we assume that list items have a meaningful comparison operation that is already defined.

* Except `append` and `insert`, all other operations are provided by ordered lists too.

---

## References

* [Lists](https://runestone.academy/runestone/books/published/pythonds/BasicDS/Lists.html)
