# To simulate the circle, we will use a queue (see Figure 3).
# Assume that the child holding the potato will be at the front
# of the queue. Upon passing the potato, the simulation will simply
# dequeue and then immediately enqueue that child, putting her at
# the end of the line. She will then wait until all the others have
# been at the front before it will be her turn again.
# After num dequeue/enqueue operations, the child at the front
# will be removed permanently and another cycle will begin.
# This process will continue until only one name remains
# (the size of the queue is 1).

from typing import List

from simple_queue_imp import SimpleQueueImp


def play_hot_potato(names: List[str], passes: int, shuffle: bool = False) -> str:
    if not names or len(names) < 2:
        raise ValueError("Names should have atleast 2 entries")
    if passes <= 0:
        raise ValueError("passes cannot be <= 0")

    participants = SimpleQueueImp()

    for name in names:
        participants.enqueue(name)

    while participants.size() > 1:
        for _ in range(passes):
            # during each pass add the first to last
            # This would seem like the object is at index 0
            # and people are moving around the object
            participants.enqueue(participants.dequeue())

        # after n passes the person at the index 0 loses
        loser = participants.dequeue()
        print(f"{loser} lost.")

    return participants.dequeue()


if __name__ == "__main__":
    print(play_hot_potato(["Bill", "David", "Susan", "Jane", "Kent", "Brad"], 7))
