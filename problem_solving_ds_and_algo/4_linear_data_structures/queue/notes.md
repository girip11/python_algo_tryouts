# Queue

> A queue is an ordered collection of items where the addition of new items happens at one end, called the “rear,” and the removal of existing items occurs at the other end, commonly called the “front.” As an element enters the queue it starts at the rear and makes its way toward the front, waiting until that time when it is the next element to be removed.
> The item that has been in the collection the longest is at the front. This ordering principle is sometimes called FIFO, first-in first-out. It is also known as “first-come first-served.”

## Abstract data type

* `enqueue(item)`
* `dequeue`
* `isEmpty`
* `size`

---

## References

* [Queue data structure](https://runestone.academy/runestone/books/published/pythonds/BasicDS/WhatIsaQueue.html)
