from abc import ABC, abstractmethod
from typing import Any


class Queue(ABC):
    @abstractmethod
    def enqueue(self, item: Any):
        pass

    @abstractmethod
    def dequeue(self) -> Any:
        pass

    @abstractmethod
    def isEmpty(self) -> bool:
        pass

    @abstractmethod
    def size(self) -> int:
        pass


# The problem of using list internally for the Queue
# is the dequeue operation will be O(n)
class SimpleQueueImp(Queue):
    def __init__(self):
        self._queue = []

    def enqueue(self, item):
        self._queue.append(item)

    def dequeue(self) -> Any:
        return self._queue.pop(0)

    def isEmpty(self) -> bool:
        return self.size() == 0

    def size(self) -> int:
        return len(self._queue)


# If we implement queue using linked list with a head and tail pointer
# then we can have the enqueue and dequeue operations as O(1)
