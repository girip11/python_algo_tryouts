from typing import List


def convert_decimal_to_base(number: int, base: int, base_digits: List[str]) -> str:
    # assume number is a positive integer
    stack = []
    quotient = abs(number)

    while quotient != 0:
        stack.append(quotient % base)
        quotient = quotient // base

    result = ""
    for _ in range(len(stack)):
        result += base_digits[stack.pop()]

    return result


print(convert_decimal_to_base(8, 2, ["0", "1"]))
print(convert_decimal_to_base(25, 8, ["0", "1", "2", "3", "4", "5", "6", "7"]))
print(convert_decimal_to_base(256, 16, ["0", "1"]))
