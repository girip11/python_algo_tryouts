from paranthesis_checker import generic_par_checker


def convert_infix_to_postfix(infix_expr: str) -> str:
    """Convert an expression in infix notation to postfix notation.

    Infix expression can contain paranthesis. Currently only the
    following binary operators are supported +,-,* and /

    Parameters
    ----------
    infix_expr : str
        Expression in infix notation. Each token should be space separated.

    Returns
    -------
    str
        Expression in postfix notation.

    Raises
    ------
    ValueError
        When the infix_expr is None or the paranthesis in the expression is not balanced
    """
    if infix_expr is None:
        raise ValueError("infix_expr is None")

    if not generic_par_checker(infix_expr):
        raise ValueError("infix_expr does not have balanced paranthesis")

    postfix_notation = []
    operator_stack = []
    # paranthesis has the highest precedence
    operator_precedence = {"(": 1, "**": 2, "*": 3, "/": 3, "+": 4, "-": 4}

    for token in infix_expr.split():
        if token == ")":
            # similar to peeking in to top of the stack
            while operator_stack != [] and operator_stack[-1] != "(":
                postfix_notation.append(operator_stack.pop())

            # pop the opening paranthesis
            operator_stack.pop()
        elif token in operator_precedence.keys():
            if operator_stack != [] and operator_stack[-1] != "(":
                while (
                    operator_stack != []
                    and operator_precedence[token] >= operator_precedence[operator_stack[-1]]
                ):
                    postfix_notation.append(operator_stack.pop())

            # push the current operator to the stack
            operator_stack.append(token)
        else:
            # these must be the operands
            postfix_notation.append(token)

    while operator_stack != []:
        postfix_notation.append(operator_stack.pop())

    return " ".join(postfix_notation)


if __name__ == "__main__":
    print(convert_infix_to_postfix("a + b"))
    print(convert_infix_to_postfix("( a + b )"))
    print(convert_infix_to_postfix("( a + b ) * c"))
    print(convert_infix_to_postfix("a + b * c"))
    print(convert_infix_to_postfix("( A + B ) * ( C + D )"))
    print(convert_infix_to_postfix("5 * 3 ** ( 4 - 2 )"))
    print(convert_infix_to_postfix("A * B * C * D +  E + F"))
    print(convert_infix_to_postfix("A + ( ( B + C ) * ( D + E ) )"))
    print(convert_infix_to_postfix("( A + B ) * ( C + D ) * ( E + F )"))
