from paranthesis_checker import generic_par_checker

# Algorithm
# Traverse the expression in reverse
# Follow the rules for converting reversed expression to postfix
# with the slight modification that we only pop when incoming operator
# has lower precedence that the top of the stack.
# In postfix the check for popping isif "<=". For prefix its just "<"
# Once the entire reversed expression is converted to postfix expression
# reverse the expression again to get the prefix expression


def convert_infix_to_prefix(infix_expr: str) -> str:
    """Convert an expression in infix notation to prefix notation.

    Infix expression can contain paranthesis. Currently only the
    following binary operators are supported +,-,* and /

    Parameters
    ----------
    infix_expr : str
        Expression in infix notation. Each token should be space separated.

    Returns
    -------
    str
        Expression in prefix notation.

    Raises
    ------
    ValueError
        When the infix_expr is None or the paranthesis in the expression is not balanced
    """
    if infix_expr is None:
        raise ValueError("infix_expr is None")

    if not generic_par_checker(infix_expr):
        raise ValueError("infix_expr does not have balanced paranthesis")

    prefix_expr = []
    operators_stack = []
    # since we will be dealing with reversed infix expression
    # we push the closing paranthesis first
    # and then we look for a matching opening paranthesis.
    operator_precedence = {")": 1, "**": 2, "/": 3, "*": 3, "+": 4, "-": 4}

    for token in infix_expr.split()[::-1]:
        if token == "(":
            while operators_stack != [] and operators_stack[-1] != ")":
                prefix_expr.append(operators_stack.pop())
            operators_stack.pop()  # remove the opening paranthesis
        elif token in operator_precedence.keys():
            if operators_stack != [] and operators_stack[-1] != ")":
                while (
                    operators_stack != []
                    and operator_precedence[token] > operator_precedence[operators_stack[-1]]
                ):
                    prefix_expr.append(operators_stack.pop())
            operators_stack.append(token)
        else:
            # here we encounter operands
            prefix_expr.append(token)

    while operators_stack != []:
        prefix_expr.append(operators_stack.pop())

    return " ".join(prefix_expr[::-1])


if __name__ == "__main__":
    print(convert_infix_to_prefix("a + b"))
    print(convert_infix_to_prefix("( a + b )"))
    print(convert_infix_to_prefix("( a + b ) * c"))
    print(convert_infix_to_prefix("a + b * c"))
    print(convert_infix_to_prefix("( A + B ) * ( C + D )"))
    print(convert_infix_to_prefix("5 * 3 ** ( 4 - 2 )"))
    print(convert_infix_to_prefix("A * B * C * D +  E + F"))
    print(convert_infix_to_prefix("A + ( ( B + C ) * ( D + E ) )"))
    print(convert_infix_to_prefix("( A + B ) * ( C + D ) * ( E + F )"))
