def par_checker(text: str) -> bool:
    if text is None:
        raise ValueError("text is None")

    balanced = True

    if text == "":
        return balanced

    stack = []
    for c in text:
        if c == "(":
            stack.append(c)
        if c == ")":
            # this check help to stop the check early
            if len(stack) == 0:
                balanced = False
                break
            stack.pop()

    return balanced and len(stack) == 0


# checks for (, [, and {
def generic_par_checker(text: str) -> bool:
    if text is None:
        raise ValueError("text is None")

    if text == "":
        return True

    balanced = True
    stack = []
    matching_parantheses = {")": "(", "]": "[", "}": "{"}
    for c in text:
        if c in matching_parantheses.values():
            stack.append(c)
        if c in matching_parantheses.keys():
            # if unable to pop or popped incorrect item set balanced to False
            if len(stack) == 0 or stack.pop() != matching_parantheses[c]:
                balanced = False
                break

    return balanced and len(stack) == 0


if __name__ == "__main__":
    print(par_checker("(())"))
    print(par_checker("(()"))
    print(par_checker("())"))

    print(generic_par_checker("{ { ( [ ] [ ] ) } ( ) }"))
    print(generic_par_checker("( [ ) ]"))
    print(generic_par_checker("( ( ( ) ] ) )"))
