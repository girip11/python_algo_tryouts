from typing import List

from infix_to_postfix_converter import convert_infix_to_postfix


def evaluate_postfix_expression(postfix_expr: str) -> int:
    if not postfix_expr:
        raise ValueError("postfix_expr is None or empty")

    operand_stack: List[str] = []
    operators = ["+", "-", "*", "/", "**"]

    for token in postfix_expr.split():
        if token in operators:
            op2 = operand_stack.pop()
            op1 = operand_stack.pop()
            res = str(eval(f"{op1} {token} {op2}"))
            operand_stack.append(res)
        else:
            operand_stack.append(token)

    return operand_stack.pop()


if __name__ == "__main__":
    expr = "4 + 5 * 6"
    postfix_expr = convert_infix_to_postfix(expr)
    print(postfix_expr)
    print(evaluate_postfix_expression(postfix_expr))
    print(evaluate_postfix_expression("5 3 4 2 - ** *"))
    print(
        evaluate_postfix_expression(convert_infix_to_postfix("( 1 + 2 ) * ( 3 + 4 ) * ( 5 + 6 )"))
    )
