# Write a function revstring(mystr) that uses a stack to reverse the characters in a string.

# from stack_using_python_list import Stack


def reverse_string_using_stack(in_str: str) -> str:
    if in_str is None:
        raise ValueError("Parameter in_str is None.")

    if in_str == "":
        return in_str

    stack = []
    for c in in_str:
        stack.append(c)

    return "".join(stack.pop() for _ in range(len(in_str)))


def reverse_string(in_str: str) -> str:
    if in_str is None:
        raise ValueError("Parameter in_str is None")

    if in_str == "":
        return in_str

    return "".join(in_str[i] for i in range(len(in_str) - 1, -1, -1))


print(reverse_string_using_stack("Hello world"))
print(reverse_string("Hello world"))
