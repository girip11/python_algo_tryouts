# Stack data structure

> * A stack (sometimes called a “push-down stack”) is an ordered collection of items where the addition of new items and the removal of existing items always takes place at the same end. This end is commonly referred to as the “top.” The end opposite the top is known as the “base.”
> * This ordering principle is sometimes called LIFO, last-in first-out.
> * The base of the stack is significant since items stored in the stack that are closer to the base represent those that have been in the stack the longest. The most recently added item is the one that is in position to be removed first.
> * Reversal property of stacks - Stacks are fundamentally important, as they can be used to reverse the order of items. The order of insertion is the reverse of the order of removal.

## Stack operations

Operations on stack abstract data type.

* push
* pop
* peek
* isEmpty
* size

Abstract data types when implemented becomes data structures.

---

## References

* [Stack data structure](https://runestone.academy/runestone/books/published/pythonds/BasicDS/WhatisaStack.html)
