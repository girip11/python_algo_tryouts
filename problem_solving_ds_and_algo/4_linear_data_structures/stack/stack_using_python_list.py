from typing import Any


class Stack:
    def __init__(self):
        self._items = []

    def push(self, item: Any) -> None:
        self._items.append(item)

    def pop(self) -> Any:
        return self._items.pop()

    def peek(self) -> Any:
        return self._items[-1]

    def isEmpty(self) -> bool:
        return len(self._items) == 0

    def size(self) -> int:
        return len(self._items)
