# Readme

* [Python data structures book](https://runestone.academy/runestone/books/published/pythonds/index.html)
* [Github repository for the book](https://github.com/bnmnetp/pythonds). This github repo can also be installed as a python package using `pip install pythonds`
