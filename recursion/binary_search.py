# Consider the searching problem that consists of
# determining whether or not some number appears
# in a sorted list of n numbers

from typing import List, Optional

# returns the index in the array
def binary_search(arr: List[int], item: int) -> Optional[int]:
    if arr == []:
        return None

    middle = len(arr) // 2

    if arr[middle] == item:
        return middle

    if arr[middle] < item:
        start = middle + 1
        index = binary_search(arr[start:], item)
        return (start + index) if index is not None else index

    return binary_search(arr[0:middle], item)


print(binary_search([1, 2, 3, 4, 5, 6], 7) is None)

print(binary_search([1, 2, 3, 4, 5, 6], 6) == 5)

print(binary_search([1, 2, 3, 4, 5, 6], 1) == 0)

print(binary_search([1, 2, 3, 4, 5], 3) == 2)

print(binary_search([1, 2, 3, 4, 5], 0) is None)

print(binary_search([1, 2, 3, 4, 5, 7, 8, 9], 6) is None)

print(binary_search([1, 2, 3, 4, 5, 7, 8, 9], 9) == 7)
