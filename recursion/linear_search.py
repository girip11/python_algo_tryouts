# Consider the searching problem that consists of
# determining whether or not some number appears
# in a sorted list of n numbers

from typing import List


def linear_search(arr: List[int], item: int) -> bool:
    if arr == []:
        return False

    return arr[0] == item or linear_search(arr[1:], item)


print(linear_search([1, 2, 3, 4, 5], 5) == True)
print(linear_search([1, 2, 3, 4, 5], 6) == False)


# decrementing index will be faster compared to slicing
# each time
def linear_search_tailrec(arr: List[int], item: int) -> bool:
    if arr == []:
        return False

    if arr[0] == item:
        return True

    return linear_search(arr[1:], item)


print(linear_search_tailrec([1, 2, 3, 4, 5], 5) == True)
print(linear_search_tailrec([1, 2, 3, 4, 5], 6) == False)
