# Consider the problem of adding the elements of a
# square n × n-dimensional matrix.

from typing import List


def matrix_sum(matrix: List[List[int]]) -> int:
    if len(matrix) == 0:
        return 0

    row_sum = sum(matrix[0])

    return row_sum + matrix_sum(matrix[1:])


def matrix_sum_tailrec(matrix: List[List[int]], acc: int = 0) -> int:
    if len(matrix) == 0:
        return acc

    return matrix_sum_tailrec(matrix[1:], acc + sum(matrix[0]))


input_matrix: List[List[int]] = []
input_matrix.append(list(range(1, 4)))
input_matrix.append(list(range(4, 7)))
input_matrix.append(list(range(7, 10)))

print(matrix_sum(input_matrix) == 45)
print(matrix_sum_tailrec(input_matrix) == 45)
