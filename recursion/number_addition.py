# consider the problem of adding two nonnegative integers
# a and b by only using unit increments or decrements


def sum(a: int, b: int) -> int:
    if b == 0:
        return a

    a += 1 if (b > 0) else -1
    b += 1 if (b < 0) else -1

    return sum(a, b)

print(sum(4, 5) == 9)
print(sum(4, 0) == 4)
print(sum(4, -5) == -1)
