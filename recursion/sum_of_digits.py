# consider the problem of computing the sum of
# the digits of some positive integer n.


def digit_sum(n: int) -> int:
    if n == 0:
        return 0
    return n % 10 + digit_sum(n // 10)


print(digit_sum(100) == 1)
print(digit_sum(0) == 0)
print(digit_sum(5432) == 14)
print(digit_sum(1111111111) == 10)


def digit_sum_tailrec(n: int, s: int = 0) -> int:
    if n == 0:
        return s
    return digit_sum_tailrec(n // 10, n % 10 + s)


print(digit_sum_tailrec(100) == 1)
print(digit_sum_tailrec(0) == 0)
print(digit_sum_tailrec(5432) == 14)
print(digit_sum_tailrec(1111111111) == 10)
